#!/bin/sh

sudo dnf install --assumeyes langpacks-de
sudo localectl set-locale de_DE.UTF-8

#sudo dnf install --assumeyes https://repo.almalinux.org/almalinux/almalinux-release-latest-8.x86_64.rpm
#sudo dnf upgrade --assumeyes

# Docker
sudo dnf config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
sudo dnf install -y docker-ce
sudo systemctl start docker
sudo systemctl enable docker
sudo usermod -aG docker $USER

# Docker-Compose
sudo curl -L "https://github.com/docker/compose/releases/download/v2.10.1/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
sudo ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose

# Ohmyzsh
sudo dnf install --assumeyes git util-linux-user zsh
sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)" "" --unattended
sudo chsh $USER -s /bin/zsh

# Neofetch
sudo curl -s https://raw.githubusercontent.com/dylanaraps/neofetch/master/neofetch -o /usr/bin/neofetch
sudo chmod +x /usr/bin/neofetch
echo "neofetch --disable title" >> .bashrc
