#!/bin/bash

# https://www.how2shout.com/linux/how-to-install-openlitespeed-web-server-on-almalinux-8-rocky-linux-8/
sudo rpm -Uvh http://rpms.litespeedtech.com/centos/litespeed-repo-1.1-1.el8.noarch.rpm
sudo dnf install epel-release
sudo dnf update
# Invalid configuration value: failovermethod=priority in /etc/yum.repos.d/litespeed.repo;
# Configuration: OptionBinding with id "failovermethod" does not exist
sudo vi /etc/yum.repos.d/litespeed.repo
sudo dnf install openlitespeed

# PHP 7.4
sudo dnf install lsphp74 lsphp74-mysqlnd lsphp74-process lsphp74-bcmath lsphp74-pdo \
lsphp74-common lsphp74-xml lsphp74-mbstring lsphp74-mcrypt \
lsphp74-soap lsphp74-gd lsphp74-opcache

# PHP 8.0
sudo dnf install lsphp80 lsphp80-mysqlnd lsphp80-process lsphp80-bcmath lsphp80-pdo \
lsphp80-common lsphp80-xml lsphp80-mbstring lsphp80-gd lsphp80-opcache lsphp80-soap

sudo systemctl status lsws
sudo systemctl stop lsws
sudo systemctl start lsws
sudo systemctl restart lsws
sudo firewall-cmd --zone=public --permanent --add-port={80/tcp,443/tcp,8088/tcp,7080/tcp}
sudo firewall-cmd --reload
sudo /usr/local/lsws/admin/misc/admpass.sh